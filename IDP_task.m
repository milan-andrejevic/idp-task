
%% PSYCHTOOLBOX initiate screen
run('IDP_prepScreen.m')

%% PSYCHTOOLBOX prepare keyboard
KbName('UnifyKeyNames'); % Makes key names consistent across Macs/Windows/Linux
%response key codes in a sequence corresponding to target positions 1-8
responseKeys = KbName({'v', 'm', 'c', ',<', 'x', '.>', 'z', '/?', 'q'});
keysOfInterest=zeros(1,256);
keysOfInterest(responseKeys)=1;


%% Schedule
% task stage titles
taskStages= {'Familiarization 1', 'Familiarization 2', 'Baseline', 'Experiment'};

% PREPARE THE MAIN EXPERIMENT
% Blocks
difficultyIni = [1,2,4,6,8];
difficulty = difficultyIni(randperm(length(difficultyIni)));
[~,ind]=sort(difficulty);
[~,diffLevel]=sort(ind);

nTrials = 20;

taskMat=transpose([1:length(difficulty)*nTrials;... % trialID
    repmat(1:nTrials,1, length(difficulty));... % trial within Block
    repelem(difficulty, nTrials);... % block
    nan(1, length(difficulty)*nTrials)]);
durMat=nan(length(difficulty)*nTrials, 3); %matrix of fixed durations
timeMat=nan(length(difficulty)*nTrials, 3); %matrix of timestamps

% Timing:
%jittered fixation cross durations
fixDur = 1000;
signalDur = [1:1.5/(nTrials-1):2.5];
tesponseDeadline=2;
%[skewedDur,type] = johnsrnd([2,2.5,3,4], 123,1);
%skewedDur(skewedDur<1.5)=1.5;
%skewedDur(skewedDur>8)=8;
%hist(signalDur)
%in case difficulty is 1, determine if the position is left or right:
positionBlock = randi(2);

% Location pool (made for 20 trials)
ones(nTrials,1);

locationPool1=[
    repmat([1],20,1),...
    repmat([1;2],10,1),...
    repmat([1;2;3;4],5,1),...
    ];
locationPool2=[
    repmat([1;2;3;4;5;6],4,1),...
    repmat([1;2;3;4;5;6;7;8],3,1),...
    ];

for i = 1:length(difficulty)
    if difficulty(i)<6
        locations=locationPool1(randperm(nTrials),diffLevel(i));
    else
        excess=[1,1,1];
        %make sure that the remaining four positions that don't get shown
        %don't repeat leaving one location without target too often
        while length(unique(excess))<length(excess)
            locationsPlus=locationPool2(randperm(24),diffLevel(i)-3);
            excess=locationsPlus(20:24);
            locations=locationsPlus(1:20);
        end
    end
    taskMat(taskMat(:,3)==difficulty(i),4)=locations; %target location

    if difficulty(i)==1 %if there is only one target then give it the planned position
        taskMat(taskMat(:,3)==difficulty(i),4)=positionBlock;
    end
    durMat(taskMat(:,3)==difficulty(i),1) = 1; % time fixation cross turns red
    durMat(taskMat(:,3)==difficulty(i),2) = signalDur(randperm(nTrials)); % time the target appears
    durMat(taskMat(:,3)==difficulty(i),3) = 3; %time the new trial begins
    
end
% convert durations into timestamps
tempDurMat = durMat';
tempTimeMat = timeMat';
tempTimeMat(1)=tempDurMat(1);
for i=1:(length(tempDurMat(:))-1)
    tempTimeMat(i+1)=tempTimeMat(i)+tempDurMat(i+1);
end
timeMat=tempTimeMat';


taskMat= [taskMat,timeMat,nan(length(timeMat),6)];

%1 trialID
%2 trial within Block
%3 block
%4 target location
%5 planned time fixation cross turns red
%6 planned time the target appears
%7 planned time the screen new trial begins
%8 time the fresh deck was drawn
%9 actual time fixation cross turns red
%10 actual time the target appears
%11 response time
%12 response
%13 response correct

%% Familiarization blocks
for i = 1:2
    
    DrawFormattedText(scrID,['Familiarization',num2str(i)],'center', 'center', [0 0 0]);
    Screen('Flip', scrID)
    WaitSecs(7);
    quit=0;
    while quit==0
        
        % SCREEN 1: prepare the board - Here we always show all 8 circles
        Screen('FrameOval', scrID,  targOutline, transpose(objectRect(1:8,:)), 3,3);
        Screen('DrawLines', scrID, Par.Disp.fix.allCoords, Par.Disp.fix.lineWidthPix, Par.Disp.fix.color,  [x0, y0]); % Draw fix cross
        t0=Screen('Flip', scrID, [], 1);
        
        if i==2.
            % SCREEN 2: turn fixation cross red
            Screen('DrawLines', scrID, Par.Disp.fix.allCoords, Par.Disp.fix.lineWidthPix, Par.Disp.fix.colorSignal,  [x0, y0]); % Draw fix cross
            WaitSecs('UntilTime', t0 + 1);
            taskMat(i,9)=Screen('Flip', scrID, [], 1);
        end
        
        % SCREEN 3: show the target at a completely random position
        Screen('FillOval', scrID,  targFill, objectRect(randi(8),:));
        %introduce a random delay from the uniform signal sample
        WaitSecs('UntilTime', t0 + 1 + signalDur(randi(nTrials)));
        Screen('Flip', scrID);
        
        % READ KEYS:
        
        startSecs = GetSecs;
        ListenChar(0);
        KbQueueCreate([], keysOfInterest);
        KbQueueStart;
        
        while GetSecs <(startSecs+tesponseDeadline - 0.005)
            [pressed,  firstKeyPressTimes]=KbQueueCheck; % check the queue for key presses
            
            if pressed
                keysPressed=find(firstKeyPressTimes);
                if keysPressed(1)==responseKeys(9)
                    quit=1;
                end
                break;
            end
        end
    end
end


%% Task Loop
clear t0
quit=0;
for i = 1: length(taskMat)
    
    % SCREEN 1: prepare the board
    %if there is only one circle to be shown
    if taskMat(i,3) == 1
        %show only the circle outline for the same circle where the target is planned to appear
        Screen('FrameOval', scrID,  targOutline, objectRect(taskMat(i,4),:), 3,3);
    else % if there are multiple circles
        %show the number of circles corresponding to difficulty levels
        Screen('FrameOval', scrID,  targOutline, transpose(objectRect(1:taskMat(i,3),:)), 3,3);
    end
    %show a fixation cross in the middle of the screen 
    Screen('DrawLines', scrID, Par.Disp.fix.allCoords, Par.Disp.fix.lineWidthPix, Par.Disp.fix.color,  [x0, y0]); % Draw fix cross
    %if this is the first trial record the t0 - fMRI trigger goes here as well.
    if i==1
        WaitSecs(7);
        t0=Screen('Flip', scrID, [], 1);
        taskMat(i,8)=t0;
    else
        if ~ isnan(taskMat(i-1,13)) %if response was not given in the previous trial
            taskMat(i,8)=Screen('Flip', scrID, [], 1);
        else
            WaitSecs('UntilTime', t0 + taskMat(i-1,7));
            taskMat(i,8)=Screen('Flip', scrID, [], 1);
        end
    end
    % SCREEN 2: turn fixation cross red
    Screen('DrawLines', scrID, Par.Disp.fix.allCoords, Par.Disp.fix.lineWidthPix, Par.Disp.fix.colorSignal,  [x0, y0]); % Draw fix cross
    WaitSecs('UntilTime', t0 + taskMat(i,5));
    taskMat(i,9)=Screen('Flip', scrID, [], 1);
    
    % SCREEN 3: show the target
    Screen('FillOval', scrID,  targFill, objectRect(taskMat(i,4),:));
    WaitSecs('UntilTime', t0 + taskMat(i,6));
    taskMat(i,10)=Screen('Flip', scrID);
    
    % READ KEYS:
    
    startSecs = GetSecs;
    ListenChar(0);
    KbQueueCreate([], keysOfInterest);
    KbQueueStart;
    
    while GetSecs <(startSecs+tesponseDeadline - 0.005)
        [pressed,  firstKeyPressTimes]=KbQueueCheck; % check the queue for key presses
        
        if pressed
            keysPressed=find(firstKeyPressTimes);
            taskMat(i,12)=keysPressed(1);
            taskMat(i,11)=firstKeyPressTimes(keysPressed(1));
            if keysPressed(1)==responseKeys(9)
                quit=1;
            elseif keysPressed(1)==responseKeys(taskMat(i,4))
                taskMat(i,13)=1; %response was correct
            else
                taskMat(i,13)=0; %response was incorrect
            end
            break;
        end
    end
    if quit
        break
    end
    % Stop delivering events to the queue
    KbQueueStop;
    ListenChar(2);
    
end
taskMat(:,8:11)=taskMat(:,8:11)-taskMat(1,8);
% Close the Psychtoolbox window

sca;

save('results.mat', 'taskMat');  
