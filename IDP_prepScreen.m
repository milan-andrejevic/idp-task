%% SCREEN SETUP

% Enter psychtoolbox screen debug mode
Switch.debug = 0;
% Use one screen or two (0 = 1 screen / 1 = 2 screens)
Switch.scrNum = 0;
% Perform psychToolbox screen refresh rate measurements?
Switch.scrRefresh = 0; % 0 = No / 1 = Yes

% Display setup and calibration (needs to be before experimental parameters section
% because we use x0 and y0 when defining parameters)

% *** Adjustable parameters ***

% Background colour (RGB value)
Par.Disp.backgroundColour = [0 0 0];

Par.Disp.textSize = 32; % Size of instructions text
Par.Disp.textFont = 'Arial'; % Font for instructions text

% fix cross parameters
Par.Disp.fix.armSize = 20; % in pixels
Par.Disp.fix.lineWidthPix = 4; % in pixels
Par.Disp.fix.color = [255, 255, 255];
Par.Disp.fix.colorSignal = [255, 100, 100];
Par.Disp.fix.xCoords = [-Par.Disp.fix.armSize, Par.Disp.fix.armSize, 0, 0];
Par.Disp.fix.yCoords = [0, 0, -Par.Disp.fix.armSize, Par.Disp.fix.armSize];
Par.Disp.fix.allCoords = [Par.Disp.fix.xCoords; Par.Disp.fix.yCoords];


% Desired screen resolution (width + height in pixels, Hz)
Par.Disp.desiredScreenWidth_pix = 1680;
Par.Disp.desiredScreenHeight_pix = 1050;
Par.Disp.desiredScreenRefreshRate_hz = 60; %try to get as high a refresh rate as possible for more accurate timing
Par.Disp.desiredPixelSize = 32; % Pixel size in bits
% If the above settings don't work, enter ResolutionTest into command line to
% see list of supported resolutions for the monitor.

% *** End adjustable parameters ***

if Switch.scrRefresh == 0 % if screen refresh measurements are disabled
    Screen('Preference', 'SkipSyncTests', 1);
end

% Extra specifications for the monitor
PsychImaging('PrepareConfiguration');
PsychImaging('Addtask', 'General','FloatingPoint32BitIfPossible'); % set up a floating point 32 bit frame buffer

% Set screen resolution
try % If using the CRT monitor (Sony Trinitron Multiscan G240)
    
    Par.Disp.screenResolution = SetResolution(Switch.scrNum, ...
        Par.Disp.desiredScreenWidth_pix, ...
        Par.Disp.desiredScreenHeight_pix, ...
        Par.Disp.desiredScreenRefreshRate_hz, ...
        Par.Disp.desiredPixelSize);
    % If the above code doesn't work, enter ResolutionTest into command line to
    % see list of supported resolutions for the monitor.
    
catch % If this doesn't work on other computers, then find closest resolution
    
    tempScrRes = NearestResolution(Switch.scrNum, ...
        [Par.Disp.desiredScreenWidth_pix, ...
        Par.Disp.desiredScreenHeight_pix, ...
        Par.Disp.desiredScreenRefreshRate_hz, ...
        Par.Disp.desiredPixelSize]);
    
    Par.Disp.screenResolution = SetResolution(Switch.scrNum, ...
        tempScrRes.width, ...
        tempScrRes.height, ...
        tempScrRes.hz, ...
        tempScrRes.pixelSize);
    
    clear tempScrRes;
    
end % of try/catch

% If debug is on enter psychtoolbox in the transparent mode
if Switch.debug==1
    PsychDebugWindowConfiguration(0,0.5); %the second parameter here defines transparency
end



% Turn off key presses inputting into the command line window or code
% editor (can disable during testing or if the system crashes with command + c).
ListenChar(2);


% Open a Screen window
[scrID, scrDim] = Screen('OpenWindow', Switch.scrNum, ...
    [150 150 150]); % RGB values
% scrDim(:,3) = width; scrDim(:,4) = height;


% Optimising system for running the experiment
if ~Switch.debug
    HideCursor; % Hide mouse cursor
    FlushEvents ''; % Flush events
    
    % Setting max priority to psychtoolbox to increase performance
    priorityLevel = MaxPriority(scrID);
    Priority(priorityLevel);
    clear priorityLevel;
end




Par.scrDim = scrDim; % Store screen dimensions

x0 = scrDim(:,3) / 2;      % Record screen centre abscissa
y0 = scrDim(:,4) / 2;      % Record screen centre ordinate

%% Define objects and their positions

% Set colour of circle outline
targOutline = [0, 0, 0];   
% Set colour of circle fill
targFill = [50,50,150];

targRadius = 30;
targLineWidth = [2,2];

boardMarginPerc = 15;
archRadius = x0-x0*1/boardMarginPerc;
posCount=8;

%posFree is a matrix with columns:
%-posFree(:,1) x coordinates for the middle of the object
%-posFree(:,2) y coordinates for the middle of the object
% in a free coordinate system
posFree=nan(posCount,2);
for pos = 1:posCount
    posFree(pos,:) = [cos(pi/(posCount-1)*(pos-1))  sin(pi/(posCount-1)*(pos-1))];
end
%rearange positions so that: 7 5 3 1 2 4 6 8
%right now they are 8 7 6 5 4 3 2 1
posFree=posFree([5 4 6 3 7 2 8 1],:); 

% translate to the screen coordinate system whose onset is the upper left
% corner of the screen and whose units are pixels within the screen
% resolution
posScreen = [(posFree(:,1)+1)* archRadius + x0*1/boardMarginPerc,...
    ((posFree(:,2)*-1)+1) * archRadius + x0*1/boardMarginPerc ];

%plot(posScreen(:,1), posScreen(:,2))
% rect argument for each object defining the start and the end height and
% width coordinates for the object following this convention:
% [top-left-x top-left-y bottom-right-x bottom-right-y]
objectRect = [ posScreen(:,1) - targRadius  posScreen(:,2) - targRadius posScreen(:,1) + targRadius  posScreen(:,2) + targRadius ];
